package ru.poas.heat_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.poas.heat_map.dto.session.SessionDto;
import ru.poas.heat_map.exception.SessionNotFoundException;
import ru.poas.heat_map.service.SessionService;

@RestController
@RequestMapping(value = "/session")
public class SessionController {

    private SessionService sessionService;

    @Autowired
    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }
    @GetMapping("/get/{id}")
    public ResponseEntity getSession(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(sessionService.getSession(id), HttpStatus.OK);
        }
        catch (SessionNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/get/all")
    public ResponseEntity getAllSessions() {
        try {
            return new ResponseEntity<>(sessionService.getAllSessions(), HttpStatus.OK);
        }
        catch (SessionNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/get/allOnSite/{siteId}")
    public ResponseEntity getAllOnSite(@PathVariable(value = "siteId") Long siteId) {
        try {
            return new ResponseEntity<>(sessionService.getSessionsBySiteId(siteId), HttpStatus.OK);
        }
        catch (SessionNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/add")
    public ResponseEntity create(@RequestBody SessionDto sessionDto) {
        try {
            return new ResponseEntity<>(sessionService.createSession(sessionDto), HttpStatus.CREATED);
        }
        catch (DataIntegrityViolationException e) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable(value = "id") Long id) {
        sessionService.delete(id);
    }


}
