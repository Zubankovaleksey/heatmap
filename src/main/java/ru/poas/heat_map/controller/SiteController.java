package ru.poas.heat_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.poas.heat_map.dto.SiteDto;
import ru.poas.heat_map.dto.UserDto;
import ru.poas.heat_map.exception.SiteNotFoundException;
import ru.poas.heat_map.exception.StorageFileNotFoundException;
import ru.poas.heat_map.service.SiteService;
import ru.poas.heat_map.service.StorageService;

@Controller
@RequestMapping(value = "/site")
public class SiteController {

    private final StorageService storageService;
    private final SiteService siteService;

    @Autowired
    public SiteController(StorageService storageService, SiteService siteService) {
        this.storageService = storageService;
        this.siteService = siteService;
    }

    @GetMapping(value = "/files/{filename:.+}", produces =  MediaType.ALL_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/add")
    public ResponseEntity addSite(@RequestBody SiteDto siteDto) {
        try {
            return new ResponseEntity<>(siteService.addSite(siteDto), HttpStatus.CREATED);
        }
        catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping("/get/{id}")
    public ResponseEntity getSite(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(siteService.getSite(id), HttpStatus.OK);
        }
        catch (SiteNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    /*@GetMapping("/getPage/{id}")
    public ResponseEntity getPageSite(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(siteService.getSite(id), HttpStatus.OK);
        }
        catch (SiteNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }*/

    @GetMapping("/get/all")
    public ResponseEntity getAllSites() {
        try {
            return new ResponseEntity<>(siteService.getAllSites(), HttpStatus.OK);
        }
        catch (SiteNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable(value = "id") Long id) {
        siteService.delete(id);
    }

}
