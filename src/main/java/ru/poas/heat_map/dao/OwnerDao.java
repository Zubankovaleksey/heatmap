package ru.poas.heat_map.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.poas.heat_map.model.Owner;

import java.util.Optional;

@Repository
public interface OwnerDao extends CrudRepository<Owner, Long> {
    Optional<Owner> findByLogin(String login);
}
