package ru.poas.heat_map.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.poas.heat_map.model.Site;

import java.util.Optional;

@Repository
public interface SiteDao extends CrudRepository<Site, Long> {
    Optional<Site> findByName(String name);
}
