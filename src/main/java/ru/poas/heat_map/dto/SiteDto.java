package ru.poas.heat_map.dto;

import lombok.Getter;
import lombok.Setter;
import ru.poas.heat_map.model.Site;

public class SiteDto {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String fileLocation;

    public SiteDto(Site site) {
        this.id = site.getId();
        this.name = site.getName();
        this.fileLocation = site.getFileLocation();
    }

    public SiteDto(Long id, String name, String fileLocation) {
        this.id = id;
        this.name = name;
        this.fileLocation = fileLocation;
    }
}
