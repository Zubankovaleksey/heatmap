package ru.poas.heat_map.dto.session;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.poas.heat_map.model.session.SessionFrame;

@NoArgsConstructor
@ToString
public class SessionFrameDto {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private Long sessionId;

    @Getter
    @Setter
    private Long pageId;

    @Getter
    @Setter
    private Long time;

    @Getter
    @Setter
    private Double first;

    @Getter
    @Setter
    private Double second;

    @Getter
    @Setter
    private Double third;

    @Getter
    @Setter
    private Double firstCursor;

    @Getter
    @Setter
    private Double secondCursor;

    @Getter
    @Setter
    private Double thirdCursor;


    public SessionFrameDto(SessionFrame sessionFrame) {
        this.id = sessionFrame.getId();
        this.sessionId = sessionFrame.getSessionId();
        this.pageId = sessionFrame.getPageId();
        this.time = sessionFrame.getTime();
        this.first = sessionFrame.getFirst();
        this.second = sessionFrame.getSecond();
        this.third = sessionFrame.getThird();
        this.firstCursor = sessionFrame.getFirstCursor();
        this.secondCursor = sessionFrame.getSecondCursor();
        this.thirdCursor = sessionFrame.getThirdCursor();
    }
}
