package ru.poas.heat_map.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.poas.heat_map.dto.SiteDto;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Table(name = "Site")
public class Site {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name="NAME", nullable = false)
    private String name;

    @Getter
    @Setter
    @Column(name="URL", nullable = false)
    private String fileLocation;

    public Site(SiteDto siteDto) {
        this.name = siteDto.getName();
        this.fileLocation = siteDto.getFileLocation();
    }

    public Site(String name, String fileLocation) {
        this.name = name;
        this.fileLocation = fileLocation;
    }
}
