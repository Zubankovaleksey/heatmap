package ru.poas.heat_map.model.session;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.poas.heat_map.dto.session.SessionDto;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "session", uniqueConstraints = @UniqueConstraint(columnNames = {"NAME"}))
public class Session {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name="USER_ID", nullable = false)
    private Long userId;

    @Getter
    @Setter
    @Column(name="NAME", nullable = false)
    private String name;

    @Getter
    @Setter
    @Column(name="PAGE_SHOW_TIME", nullable = false)
    private int pageShowTime;

    @Getter
    @Setter
    @Column(name="SITE_ID", nullable = false)
    private Long siteId;

    public Session(SessionDto sessionDto) {
        this.userId = sessionDto.getUserId();
        this.name = sessionDto.getName();
        this.pageShowTime = sessionDto.getPageShowTime();
        this.siteId = sessionDto.getSiteId();
    }
}
