package ru.poas.heat_map.model.session;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.poas.heat_map.dto.session.SessionFrameDto;

import javax.persistence.*;

@NoArgsConstructor
@Entity
public class SessionFrame {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name = "SESSION_ID")
    private Long sessionId;

    @Getter
    @Setter
    @Column(name = "PAGE_ID")
    private Long pageId;

    @Getter
    @Setter
    @Column(name = "TIME")
    private Long time;

    @Getter
    @Setter
    @Column(name = "FIRST_AREA")
    private Double first;

    @Getter
    @Setter
    @Column(name = "SECOND_AREA")
    private Double second;

    @Getter
    @Setter
    @Column(name = "THIRD_AREA")
    private Double third;

    @Getter
    @Setter
    @Column(name = "FIRST_CURSOR")
    private Double firstCursor;

    @Getter
    @Setter
    @Column(name = "SECOND_CURSOR")
    private Double secondCursor;

    @Getter
    @Setter
    @Column(name = "THIRD_CURSOR")
    private Double thirdCursor;

    public SessionFrame(SessionFrameDto sessionFrameDto) {
        this.sessionId = sessionFrameDto.getSessionId();
        this.pageId = sessionFrameDto.getPageId();
        this.time = sessionFrameDto.getTime();
        this.first = sessionFrameDto.getFirst();
        this.second = sessionFrameDto.getSecond();
        this.third = sessionFrameDto.getThird();
        this.firstCursor = sessionFrameDto.getFirstCursor();
        this.secondCursor = sessionFrameDto.getSecondCursor();
        this.thirdCursor = sessionFrameDto.getThirdCursor();
    }
}
