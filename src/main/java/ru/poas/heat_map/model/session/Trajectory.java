package ru.poas.heat_map.model.session;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.minidev.json.JSONArray;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class Trajectory {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name = "SESSION_FRAME_ID")
    private Long sessionFrameId;

    @Getter
    @Setter
    @Type( type = "json" )
    @Column(name = "POINTS", columnDefinition = "json" )
    private JSONArray points;
}
