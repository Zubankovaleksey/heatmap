package ru.poas.heat_map.service;

import ru.poas.heat_map.dao.OwnerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.poas.heat_map.model.Owner;
import java.util.ArrayList;

@Service
public class OwnerDetailsServiceImpl implements UserDetailsService {

    private OwnerDao ownerDao;

    @Autowired
    public OwnerDetailsServiceImpl(OwnerDao ownerDao) {
        this.ownerDao = ownerDao;
    }

    @Override
    public UserDetails loadUserByUsername(String login) {
        Owner user = ownerDao.findByLogin(login).orElseThrow(() -> new UsernameNotFoundException("Invalid login"));
        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), new ArrayList<>());
    }
}
