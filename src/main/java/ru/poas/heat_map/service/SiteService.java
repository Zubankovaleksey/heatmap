package ru.poas.heat_map.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.poas.heat_map.dao.SiteDao;
import ru.poas.heat_map.dto.SiteDto;
import ru.poas.heat_map.dto.UserDto;
import ru.poas.heat_map.exception.SiteNotFoundException;
import ru.poas.heat_map.model.Site;
import ru.poas.heat_map.model.User;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
public class SiteService {

    private StorageService storageService;
    private SiteDao siteDao;

    @Autowired
    public SiteService(SiteDao siteDao, StorageService storageService) {
        this.siteDao = siteDao;
        this.storageService = storageService;
    }

    @Transactional
    public SiteDto saveSite(MultipartFile file){

        storageService.store(file);
        Site site= new Site(file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf(".")), file.getOriginalFilename());
        siteDao.save(site);
        return new SiteDto(site);
    }


    public List<SiteDto> getAllSites() {
        List<SiteDto> sites = new ArrayList<>();
        siteDao.findAll().forEach(e-> sites.add(new SiteDto(e)));
        if (sites.isEmpty())
            throw new SiteNotFoundException("Site list is empty.");
        return sites;
    }
    public SiteDto addSite(SiteDto siteDto)
    {
        return new SiteDto(siteDao.save(new Site(siteDto)));
    }

    public SiteDto getSite(Long id) {
        return new SiteDto(siteDao.findById(id)
                .orElseThrow(()-> new SiteNotFoundException("Site with id " + id + " not found.")));
    }


    public void delete(Long id) {
        siteDao.deleteById(id);
    }
}
